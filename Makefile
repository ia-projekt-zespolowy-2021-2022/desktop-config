PREFIX ?= /usr
DOCDIR ?= $(PREFIX)/share/doc/desktopconfig
MANDIR ?= $(PREFIX)/share/man/man1
LIBDIR ?= $(PREFIX)/lib/desktopconfig
CONFDIR ?= $(PREFIX)/share/desktopconfig

all:
	@echo Run \'make install\' to install desktopconfig
	@echo Run \'make uninstall\' to uninstall desktopconfig

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@cp -p desktopconfig $(DESTDIR)$(PREFIX)/bin/desktopconfig
	@mkdir -p $(DESTDIR)$(DOCDIR)
	@cp -p README.md $(DESTDIR)$(DOCDIR)
	@cp -p documentation/gnome.md $(DESTDIR)$(DOCDIR)
	@cp -p documentation/xfce.md $(DESTDIR)$(DOCDIR)
	@mkdir -p $(DESTDIR)$(MANDIR)
	@cp -p manual/desktopconfig.1.gz $(DESTDIR)$(MANDIR)
	@mkdir -p $(DESTDIR)$(LIBDIR)
	@cp -a scripts/ $(DESTDIR)$(LIBDIR)
	@mkdir -p $(DESTDIR)$(CONFDIR)
	@cp -a configuration/ $(DESTDIR)$(CONFDIR)

	@chmod 755 $(DESTDIR)$(PREFIX)/bin/desktopconfig
	

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/bin/desktopconfig
	@rm -rf $(DESTDIR)$(DOCDIR)
	@rm -rf $(DESTDIR)$(MANDIR)/desktopconfig.1.gz
	@rm -rf $(DESTDIR)$(LIBDIR)
	@rm -rf $(DESTDIR)$(CONFDIR)
