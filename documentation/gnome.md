# GNOME configuration files descriptions

## Introduction
In this file user can find descriptions for all keys and values used in GNOME configuration script. Be aware that when a key is present in `.conf` but it has no value, then it is set to it's default value.


## Terminal

### bell
Terminal bell. Can be set to `true` or `false`.

### background-color
Color of terminal background. Can be set to hex color code in single quotes, i.e. `'#000000'` for black. To see changes, set `theme-colors=false`.

### foreground-color
Color of terminal foreground (text). Can be set to hex color code in single quotes, i.e. `'#000000'` for black. To see changes, set `theme-colors=false`.

### theme-colors
Usage of theme colors. Can be set to `true` or `false`.

### cursor-custom-color
Usage of custom cursor color. Can be set to hex color code in single quotes. To see changes, set `theme-colors=false`.

### cursor-background-color
Color of cursor background. Can be set to hex color code in single quotes. To see changes, set `theme-colors=false` and `theme-colors=false`.

### cursor-foreground-color
Color of cursor foreground. Can be set to hex color code in single quotes. To see changes, set `theme-colors=false` and `theme-colors=false`.

### cursor-blink
Cursor blinking mode. Can be set to `'on'`, `'off'` or `'system'`.

### cursor-shape
Cursor shape. Can be set to `'block'`, `'ibeam'` or `'underline'`.

### encoding
Encoding used in terminal. Can be set to any available encoding, i.e. `'UTF8'` or `'ISO-8859-14'`.

### font
Terminal font. Can be set to any available font, i.e. `'Monospace 12'`, `'Liberation Mono 14'`. To see changes, set `system-font=false`.

### system-font
Usage of system font. Can be set to `false` or `true`.

### scrollbar
Show scrollbar. Can be set to `'always'` or `'never'`.

### scroll-on-keystroke
Scrolling of keystroke. Can be set to `false` or `true`.

### scroll-on-output
Scrolling of keystroke. Can be set to `false` or `true`.

### size-columns
Initial number of columns. Can be set to an integer from 16 to 511.

### size-rows
Initial number of rows. Can be set to an integer from 4 to 511.


## Panel

### battery-percentage
Show battery percentage. Can be set to `false` or `true`.

### clock-date
Show date in clock. Can be set to `false` or `true`.

### clock-weekday
Show weekday in clock. Can be set to `false` or `true`.

### clock-seconds
Show seconds in clock. Can be set to `false` or `true`.

### clock-format
Whether the clock displays in 24h or 12h format. Can be set to `'24h'` or `'12h'`.


## Desktop

### wallpaper
Path to wallpaper image. Can be set to any string that is a path of a image.

### wallpaper-mode
Wallpaper mode - determines how image is rendered. Can be set to `'none'`, `'wallpaper'`, `'centered'`, `'scaled'`, `'stretched'`, `'zoom'` or `'spanned'`.

### screensaver
Path to screensaver image. Can be set to any string that is a path of a image.

### screensaver-mode
Screensaver mode - determines how image is rendered. Can be set to `'none'`, `'wallpaper'`, `'centered'`, `'scaled'`, `'stretched'`, `'zoom'` or `'spanned'`.

### cursor-size
Size of cursor. Can be set to any integer from -2,147,483,648 to 2,147,483,647.

