# XFCE configuration files descriptions

## Introduction
In this file user can find descriptions for all keys and values used in XFCE configuration script. Be aware that when a key is present in `.conf` but it has no value, then it is set to it's default value.

## Desktop

Please note that the changes will be visible for all monitors and workspaces

### color-style
Style of the color drawn behind the backdrop image. Can set to number:
* 0 - Solid color
* 1 - Horizontal gradient
* 2 - Vertical gradient
* 3 - Transparent

### image-style
Specify how the image will be resized to fit the screen. Can set to number:
* 0 - None
* 1 - Centred
* 2 - Tiled
* 3 - Stretched
* 4 - Scaled
* 5 - Zoomed

### last-image
Specify wallpaper for the desktop. Provide path to the file.

## Panels

### dark-mode
Can be set to `true` or `false`.

### up-icon-size
Set the icon size at the top of the screen. Accepts an integer.

### up-length/down-length
Set the length of the panel at the top/bottom of the screen. Accepts an integer.

### up-position/down-position
Set the position of the panel at the top/bottom of the screen. `down-position="p=10;x=0;y=0"`

### up-position-locked/down-position-locked
Set if the position of the panel should be locked. `true` or `false`

### up-size/down-size
Set the size of the panel. Accepts an integer.

### down-panel-show
`true` to show panel-2; `false` to hide it.

### down-autohide-behavior
Set if the panel below the screen should autohide. Accepts a number:
* 0 - Never
* 1 - Intelligently
* 2 - Always

## Terminal

### ColorBackground
Set the background color. Accepts hex colors i.e. `#000000` for black.

### ColorForeground
Set the backgorund color. Accepts hex colors i.e. `#000000` for black.

### FontName
Set the font name.

### MiscAlwaysShowTabs
Specify if the tab headers shoul be always displayed. `TRUE` or `FALSE`

### MiscBell
Enable the audible bell. `TRUE` or `FALSE`

### MiscConfirmClose
Show the dialog window when user tries to close the window. `TRUE` or `FALSE`

### MiscCursorBlinks
Cursor blinks. `TRUE` or `FALSE`

### MiscCursorShape
Set the shape of the cursor. Available options: `TERMINAL_CURSOR_SHAPE_BLOCK`, `TERMINAL_CURSOR_SHAPE_IBEAM`, `TERMINAL_CURSOR_SHAPE_UNDERLINE`.

### MiscCycleTabs
Enable circulating through the tabs (from last to first) using `Next button`, `TRUE` or `FALSE`

### MiscDefaultGeometry
The default geometry of the new window.

### MiscInheritGeometry
Enable inheriting geometry from parent window. `TRUE` or `FALSE`

### MiscMouseAutohide
Hide the mouse cursor. `TRUE` or `FALSE`

### MiscTabCloseButtons
Show the close button with tab bar. `TRUE` or `FALSE`

### MiscTabCloseMiddleClick
Close the tab if user middle-clicks the tab tile. `TRUE` or `FALSE`

### MiscTabPosition
Specifies where the tab headers should be displayed. Available options: `GTK_POS_TOP`, `GTK_POS_LEFT`, `GTK_POS_BOTTOM` or `GTK_POS_RIGHT`.

### MiscHighlightUrls
Highlight url links. `TRUE` or `FALSE`
