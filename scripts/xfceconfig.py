#!/usr/bin/env python3
import configparser
import itertools
import os
import shutil
import subprocess

from pathlib import Path

def _get_monitor_and_workspace_names():
    result = subprocess.check_output(['xfconf-query', '-c', 'xfce4-desktop', '-l']).decode('utf-8')
    result = result.splitlines()
    # print(result)
    monitors = list()
    workspaces = list()
    for m in result:
        m = m.split('/')
        if len(m) > 4:
            monitors.append(m[3])
            workspaces.append(m[4])
    monitors = list(dict.fromkeys(monitors))
    workspaces = list(dict.fromkeys(workspaces))

    return monitors, workspaces

def _get_mapping():
    config = configparser.ConfigParser()
    config.optionxform = str
    
    configuration_dir_path = Path("/usr/share/desktopconfig/configuration")
    config.read(configuration_dir_path / 'xfce_option_mapping.conf')

    section_keys_mapping = dict()

    for section in config.sections():
        key_value_mapping = dict()
        for key in config[section]:
            key_value_mapping[key] = config[section][key]

        section_keys_mapping[section] = key_value_mapping
    
    return section_keys_mapping

def _use_xfce_conf_query(xfce_property, config_dir):
    mapping = _get_mapping()

    config = configparser.ConfigParser()
    config.optionxform = str
    config.read(config_dir)

    for section in config.sections():
        change_cnt = 0
        key_value_mapping = mapping[section]

        if xfce_property == "xfce4-desktop":
            for key in config[section]:
                monitors, workspaces = _get_monitor_and_workspace_names()
                for m, w in itertools.product(monitors, workspaces):
                    prop = key_value_mapping[key].replace('monitorName', m)
                    prop = prop.replace('workspace', w)
                    command = f"xfconf-query -c {xfce_property} -p {prop} -s {config[section][key]}"
                    os.system(command)
                change_cnt += 1
        else:
            for key in config[section]:
                if key == "down-panel-show":
                    if config[section][key] == "true":
                        value = "--force-array -t int -s 1 -t int -s 2"
                    else:
                        value = "--force-array -t int -s 1"
                    command = f"xfconf-query -c {xfce_property} -p /panels {value}"
                    os.system(command)
                else:
                    command = f"xfconf-query -c {xfce_property} -p {key_value_mapping[key]} -s {config[section][key]}"
                    os.system(command)
                change_cnt += 1

        if xfce_property == "xfce4-panel":
            os.system("xfce4-panel -r")

        if change_cnt > 0:
            print(f"Succesfully applied {change_cnt} changes to {xfce_property} - {section}")

def configure_desktop(config_path):
    if config_path == None:
        print("Desktop configuration file not provided. Skipping.")
        return
    _use_xfce_conf_query('xfce4-desktop', config_path)

def configure_panels(config_path):
    if config_path == None:
        print("Panels configuration file not provided. Skipping.")
        return
    _use_xfce_conf_query('xfce4-panel', config_path)

def configure_terminal(config_path):
    if config_path == None:
        print("Terminal configuration file not provided. Skipping.")
        return
    # xfce4-terminal config cannot be changed via xfconf-query
    HOME_DIR = os.environ.get("HOME")
    terminal_dir_path = Path(HOME_DIR) / ".config" / "xfce4" / "terminal"
    terminal_dir_path.mkdir(parents=True, exist_ok=True)

    terminalrc_path = terminal_dir_path / "terminalrc"
    shutil.copy(config_path, terminalrc_path)

if __name__ == "__main__":
    pass
    