#!/usr/bin/env python3
import configparser
import os
import subprocess

from pathlib import Path

def _get_mapping():
    config = configparser.ConfigParser()
    config.optionxform = str

    configuration_dir_path = Path("/usr/share/desktopconfig/configuration")
    config.read(configuration_dir_path / 'gnome_option_mapping.conf')

    section_keys_mapping = dict()

    for section in config.sections():
        key_value_mapping = dict()
        for key in config[section]:
            key_value_mapping[key] = config[section][key]

        section_keys_mapping[section] = key_value_mapping

    return section_keys_mapping

def _get_schemas():
    config = configparser.ConfigParser()
    config.optionxform = str

    schema_dir_path = Path("/usr/share/desktopconfig/configuration")
    config.read(schema_dir_path / 'gnome_schemas.conf')

    section_keys_mapping = dict()

    for section in config.sections():
        key_value_mapping = dict()
        for key in config[section]:
            key_value_mapping[key] = config[section][key]

        section_keys_mapping[section] = key_value_mapping

    return section_keys_mapping


def _use_gsettings(gnome_property, schema, config_dir):
    mapping = _get_mapping()
    schemas = _get_schemas()
    schema_provided = False if schema is None else True

    config = configparser.ConfigParser()
    config.optionxform = str
    config.read(config_dir)

    for section in config.sections():
        key_value_mapping = mapping[section]

        for key in config[section]:
            if schema_provided == False:
                schema = schemas[section][key]

            command = ''
            if config[section][key] == '':
                command = f'gsettings reset {schema} {key_value_mapping[key]}'
            else:
                command = f'gsettings set {schema} {key_value_mapping[key]} {config[section][key]}'

            print(command)
            os.system(command)

def configure_desktop(config_path):
    if config_path == None:
        print("Desktop configuration file not provided. Skipping.")
        return
    
    _use_gsettings('gnome-panel', None, config_path)

def configure_panels(config_path):
    if config_path == None:
        print("Panels configuration file not provided. Skipping.")
        return

    schema = 'org.gnome.desktop.interface'

    _use_gsettings('gnome-panel', schema, config_path)

def configure_terminal(config_path):
    if config_path == None:
        print("Terminal configuration file not provided. Skipping.")
        return

    subproc = subprocess.Popen("gsettings get org.gnome.Terminal.ProfilesList default", shell=True, stdout=subprocess.PIPE)
    terminal_profile = subproc.stdout.read().decode("utf-8")
    terminal_profile = terminal_profile[1:-2]

    schema = f'org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:\'{terminal_profile}\'/'

    _use_gsettings('gnome-terminal', schema, config_path)

if __name__ == "__main__":
    pass
