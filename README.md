# XFCE and GNOME configuration scripts

## Introduction
Simple Python project that lets you configure properties of panels, desktop and terminal in both XFCE and GNOME. Project created for 'Programming Project' course at the Department of Theoretical Computer Science, Jagiellonian University.

## Technologies
* Python 3

## Setup
```sh
$ yay -S desktop-config-git
```

## desktopconfig

`desktopconfig` options | Description
------------------|------------
`ENV` | Specifies which graphic environment will be configured. Available options are `xfce` and `gnome`. Argument required. Example: `desktopconfig xfce`.
`-d` or `--desktop` | Path to desktop configuration file. If not provided the script skips desktop configuration. Example: `desktopconfig xfce -d /path/to/desktop/config`.
`-p` or `--panels` | Path to desktop configuration file. If not provided the script skips desktop configuration. Example: `desktopconfig xfce -p /path/to/panel/config`.
`-t` or `--terminal` | Path to desktop configuration file. If not provided the script skips desktop configuration. Example: `desktopconfig xfce -d /path/to/terminal/config`.

Be aware that the config file you provide must follow the particular format and variable names. You will find the tamplate in `/usr/share/desktopconfig/configuration` directory.

## Configuration file format

The project uses custom config files with custom variable names which are mapped to xfce and gnome names in `xfce_option_mapping.conf` and `gnome_option_mapping.conf` respectively. The only exception is xfce treminal configuration which isn't mapped because the only way to change terminal apperance is via replacing its config file.

You can find the documentation of all the available options in `documentation` directory.

## Adding options

The project supports a number of options to configure in both XFCE and GNOME. But not all of them. If you want to configure some properties which are not supported by default, modify a respective mapping file in `configuration` directory and add your option to the config file you provide.
